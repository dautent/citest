import {persist} from "mobx-persist";
import {action, observable} from "mobx";

export class MapStore {
    @persist('map', Item) @observable items = observable.map({})
    @action test(key = 'test') {
        console.warn(this.items.keys().join('.'))
        this.items.set(key, new Item)
    }
}
