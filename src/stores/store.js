import {observable, action, asMap, extendObservable} from 'mobx'
import {persist} from 'mobx-persist'
import {ToastAndroid} from 'react-native'


export class CountStore {
    @persist @observable count = 0

    @action inc() {
        this.count = this.count + 1
    }
}

class Item {
    @persist @observable info = 0
}

export class MapStore {
    @persist('object') @observable items = extendObservable(new Item(),{})

    @action.bound set = () => {
        this.items.info = 666
    }
    @action.bound login = () => {
        this.items.info += 1
    }
}

export class ListStore {
    @persist('list') @observable list = []

    @action test(text = `${Date.now()}`) {
        this.list.push({text})
    }
}