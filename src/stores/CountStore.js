import {persist} from "mobx-persist";
import {action, observable} from "mobx";


export class CountStore {
    @persist @observable count = 0
    @action inc() {
        this.count = this.count + 1
    }
}
