import {persist} from "mobx-persist";
import {action, observable} from "mobx";

export class ListStore {
    @persist('list') @observable list = []
    @action test(text = `${Date.now()}`) {
        this.list.push({ text })
    }
}