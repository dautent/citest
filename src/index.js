import * as React from 'react'
import {View, Text, AsyncStorage, StyleSheet, Button} from 'react-native'
import {observer} from 'mobx-react/native'
import {create} from 'mobx-persist'

import {CountStore, MapStore, ListStore} from './stores/store'

const hydrate = create({storage: AsyncStorage})
const countStore = new CountStore
const mapStore = new MapStore
const listStore = new ListStore
hydrate('count', countStore)
hydrate('map', mapStore)
hydrate('list', listStore)

@observer
class Test extends React.Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>Count: {mapStore.items.info}</Text>
                <View style={{height: 20}}/>
                <Button title="赋初值" onPress={() => mapStore.set()}/>
                <View style={{height: 20}}/>
                <Button title="递增" onPress={() => mapStore.login()}/>
                <View style={{height: 20}}/>
                <Button title="List" onPress={() => listStore.test()}/>
                {
                    listStore.list.map((item, index) => {
                        return <Text key={index}>{item.text}</Text>
                    })
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
})

export function App() {
    return <Test/>
}
